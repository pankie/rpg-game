/* 
 * File:   Json.cpp
 * Author: Fredrik
 * 
 */

#include "Json.hpp"



void Json::save(const std::string& filePath, const rapidjson::Document& document)
{
    
    // Allocates the memory buffer for writing the whole JSON to a string
    rapidjson::StringBuffer buffer;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
    document.Accept(writer);
    
    const std::string& json = buffer.GetString();
    
    // saves the file.
    writeFile(filePath, json);
}

const Json::Document Json::load(const std::string& filePath)
{
    Document document;
    std::string fileContents = File::readFile(filePath);
    document.Parse(fileContents.c_str());
    
    return document;
}

void Json::print(const Document& doc)
{
    rapidjson::StringBuffer buffer;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
    
    doc.Accept(writer);
    
    std::cout << buffer.GetString() << '\n';
}

void Json::print(const Value& val)
{
    rapidjson::StringBuffer buffer;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
    
    val.Accept(writer);
    
    std::cout << buffer.GetString() << '\n';
}

void Json::save()
{
    Document document;
    document.SetObject();
    
    AllocatorType& allocator = document.GetAllocator();

    Value array(rapidjson::kObjectType);
    Value mapContent(rapidjson::kObjectType);
   
    mapContent.AddMember("Name", "World", allocator);
    mapContent.AddMember("size", getVector({1280, 720}, allocator),  allocator);
    
    array.PushBack(mapContent, allocator);
    
    document.AddMember("Map", array, allocator);
    
    save("world.json", document);
}


Json::Value Json::getColor(const sf::Color& color, AllocatorType& all)
{
    Value object(rapidjson::kObjectType);
    object.AddMember("r", color.r, all);
    object.AddMember("g", color.g, all);
    object.AddMember("b", color.b, all);
    object.AddMember("a", color.a, all);
    
    return object;
}

Json::Value Json::getVector(const sf::Vector2f& vector, AllocatorType& all)
{
    Value object(rapidjson::kObjectType);
    
    object.AddMember("X", vector.x, all);
    object.AddMember("Y", vector.y, all);
    
    return object;
}

const sf::Color Json::getColor(const Value& val)
{
    sf::Color color;
    color.r = val["r"].GetInt();
    color.g = val["g"].GetInt();
    color.b = val["b"].GetInt();
    color.a = val["a"].GetInt();
    
    return color;
}

const sf::Vector2f Json::getVector(const Value& val)
{
    sf::Vector2f vector;
    vector.x = static_cast<float>(val["X"].GetDouble());
    vector.y = static_cast<float>(val["Y"].GetDouble());
    
    return vector;
}