/* 
 * File:   Map.cpp
 * Author: Fredrik
 * 
 */

#include "Map.hpp"
#include "World.hpp"

Map::Map(Player& player)
: _player(player)
, _mapcontainer()
{
    init();
}

void Map::init()
{
    loadWorlds();
    // Works. But contains much ugly code. Should be cleaned in near future.
    // saveWorlds();
    
}

void Map::loadWorlds()
{   
    
    // Loads the file to a json document object
    const Json::Document& document = _json.load("world.json");
    _mapcontainer.addMaps(document);
    
    // we load the world map as a starting area.
    _mapcontainer.loadMap(MapContainer::World);
}

void Map::saveWorlds()
{
    // Start writing document
    Json::Document document;
    document.SetObject();
    
    Json::Value array(rapidjson::kArrayType);
    
    // used for allocating memory
    Json::AllocatorType& allocator = document.GetAllocator();
    
    Json::Value map(rapidjson::kObjectType);
    
    map.AddMember("name", MapContainer::World, allocator);
    
    map.AddMember("spawn_position", 
        _json.getVector(_mapcontainer.getSpawnPosition(), allocator), allocator);
    
    map.AddMember("floor", _mapcontainer.getJsonFloor(allocator), allocator);
    
    map.AddMember("buildings", _mapcontainer.getJsonBuilding(allocator), allocator);
    
    array.PushBack(std::move(map), allocator);

    ////////////////////////
    // Store the second map.
    
    // load map to house
    _mapcontainer.loadMap(MapContainer::House);
    
    Json::Value map2(rapidjson::kObjectType);
    
    map2.AddMember("name", MapContainer::House, allocator);
    map2.AddMember("spawn_position", 
        _json.getVector(_mapcontainer.getSpawnPosition(), allocator), allocator);
    map2.AddMember("buildings", _mapcontainer.getJsonBuilding(allocator), allocator);
    
    array.PushBack(std::move(map2), allocator);
    
    document.AddMember("maps", std::move(array), allocator);
    // save to world.json
    _json.save("world.json", std::move(document));
    
    // change back to world map
    _mapcontainer.loadMap(MapContainer::World);
}


void Map::setSpawnPosition(const sf::Vector2f& pos)
{
    _spawnPosition = pos;
}

void Map::handleEvents(const sf::Event& event)
{
    switch(event.type)
    {
    case sf::Event::KeyReleased:
        if(event.key.code == sf::Keyboard::Down)
        {
            _mapcontainer.debug_toggleMap();
        }
        // if Y was pressed, reload the maps.
        else if(event.key.code == sf::Keyboard::Y)
        {
            const rapidjson::Document& doc = _json.load("world.json");
            _mapcontainer.reloadMaps(doc);
        }
        break;
    default:
        break;
    }
}

void Map::update(const sf::Time& dt)
{
    _mapcontainer.update(_player);
}

void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(_mapcontainer, states);
}