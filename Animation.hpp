
#ifndef ANIMATION_HPP
#define	ANIMATION_HPP

#include <SFML/Graphics.hpp>
#include <string>

class Animation
{
public:
    Animation();
    virtual void updateAnimationCycle(const sf::Time& dt);
    
    // Go to the next frame
    // Does nothing by default.
    virtual void nextFrame();
protected:
    // Keeps the animation cycle from not going too fast.
    sf::Time     timeFrame;
    
    // helps the animation to cycle
    sf::Vector2i source;
    
    // Stored 
    sf::Sprite   sprite;
    
};

#endif	/* ANIMATION_HPP */

