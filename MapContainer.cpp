/* 
 * File:   MapContainer.cpp
 * Author: Fredrik
 * 
 */

#include <map>

#include "MapContainer.hpp"
#include "World.hpp"

MapContainer::MapContainer()
: _spawnPosition()
{
}

void MapContainer::reloadMaps(const rapidjson::Document& document)
{
    std::cout << "reloading maps\n";
    const rapidjson::Value& a = document["maps"];
    // break the program if the document isn't an array.
    assert(a.IsArray());
    
    for(size_t i = 0; i < a.Size(); i++)
    {
        deleteMap(a[i]["name"].GetInt());
        
    }
    addMaps(document);
}

void MapContainer::deleteMap(const unsigned int& level)
{
    deleteMap(static_cast<MapContainer::LEVEL>(level));
}

void MapContainer::deleteMap(MapContainer::LEVEL level)
{
    auto found = _mapHolder.find(level);
    assert(found != _mapHolder.end());
    _mapHolder.erase(found);
    
    delete found->second.get();
    
}

void MapContainer::addMaps(const rapidjson::Document& document)
{

    ////////////////////////////////////////////////////////////////////////////
    // Build the worlds! ///////////////////////////////////////////////////////
    const rapidjson::Value& a = document["maps"];
    
    // check if it's an array.
    assert(a.IsArray());
    
    // prints and stores the buildings, floor and spawn positions to the maps
    for(size_t i = 0; i < a.Size(); i++)
    {
        // Document["maps"][0]["name"] = 0
        std::cout << "loading map Document[\"maps\"][" << i << "][\"name\"]"
                  << " = "
                  << a[i]["name"].GetInt() << '\n';
        
        // integer is basically the same as an enum in this case.
        insertMap(a[i]["name"].GetInt());
        
        // loads the map and prepare for inserting models
        loadMap(a[i]["name"].GetInt());
        
        addBuilding(a[i]);
        setSpawnPosition(a[i]);
        addFloor(a[i]);
    }
}

void MapContainer::debug_toggleMap()
{
    switch(_currentLevel)
    {
    case LEVEL::House:
        loadMap(LEVEL::World);
        break;
    case LEVEL::World:
        loadMap(LEVEL::House);
    default:
        break;
    };
}

void MapContainer::addFloor(const sf::RectangleShape& rectangle)
{
    getMap()._floor = rectangle;
}

void MapContainer::Building::addWall(const ColRectangle& wall)
{
    Wall.push_back(wall);
}

void MapContainer::Building::addWall(ColRectangle& wall)
{
    Wall.push_back(std::move(wall));
}

void MapContainer::Building::addDoor(const ColRectangle& door)
{
    Door = std::move(door);
}

void MapContainer::addBuilding(const Building& building)
{
    getMap()._buildings.push_back(building);
}

MapContainer& MapContainer::getMap() const
{
    auto found = _mapHolder.find(_currentLevel);
    assert(found != _mapHolder.end());
    
    return *found->second;
}

void MapContainer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    MapContainer& current = getMap();
    
    // draws the floor of the current selected map
    target.draw(current._floor, states);
    
    // draw each building
    for(const Building& building : current._buildings)
    {
        
        // draw all the walls inside the Building class
        for(const ColRectangle& wall : building.Wall)
        {
            target.draw(wall, states);
        }
        
        // draws the door for each building, if any.
        target.draw(building.Door, states);
    }
}

void MapContainer::addBuilding(const rapidjson::Value& val)
{
    Json json;
    Building building;
    
    const rapidjson::Value& walls = val["buildings"]["building"]["walls"];
    const rapidjson::Value& door  = val["buildings"]["building"];
    
    assert(walls.IsArray());
    
    for(rapidjson::SizeType i = 0; i < walls.Size(); i++)
    {
        ColRectangle wall;
        
        sf::Vector2f size = json.getVector(walls[i]["wall_size"]);
        wall.setSize(size);

        sf::Color color = json.getColor(walls[i]["wall_color"]);
        wall.changeColor(color);
        sf::Vector2f pos = json.getVector(walls[i]["wall_position"]);
        wall.setPosition(pos);
        
        // store wall to the building
        building.addWall(std::move(wall));
    }
    
    // store door to the building
    ColRectangle colDoor;
    const sf::Color    color    = json.getColor(door["door_color"]);
    const sf::Vector2f position = json.getVector(door["door_position"]);
    const sf::Vector2f size     = json.getVector(door["door_size"]);
    
    colDoor.changeColor(color);
    colDoor.setSize(size);
    colDoor.setPosition(position);
    
    building.addDoor(colDoor);
    
    // add the building to the current build
    addBuilding(building);
}

void MapContainer::addFloor(const rapidjson::Value& val)
{
    if(val.HasMember("floor"))
    {
        MapContainer& current = getMap();
        Json json;
        std::cout << "adding floor" << '\n';

        // set the floor size
        sf::Vector2f size = json.getVector(val["floor"]["floor_size"]);
        sf::RectangleShape floor(size);

        // set the color of the floor
        sf::Color color = json.getColor(val["floor"]["floor_color"]);

        floor.setFillColor(color);
        floor.setOrigin({floor.getSize().x / 2.f, floor.getSize().y / 2.f});

        // sets the position of the floor
        sf::Vector2f pos = json.getVector(val["floor"]["floor_position"]);
        floor.setPosition(pos);


        current._floor = floor;
    }
}

rapidjson::Value MapContainer::getJsonFloor(rapidjson::Document::AllocatorType& allocator)
{
    MapContainer& current = getMap();
    Json json;
    
    rapidjson::Value floorObj(rapidjson::kObjectType);
    
    floorObj.AddMember(
        "floor_size", 
        json.getVector(current._floor.getSize(), allocator), allocator);
    floorObj.AddMember(
        "floor_position", 
        json.getVector(current._floor.getPosition(), allocator), allocator);
    floorObj.AddMember(
        "floor_color", 
        json.getColor(current._floor.getFillColor(), allocator), allocator);
    
    return floorObj;
}

rapidjson::Value MapContainer::getJsonBuilding(rapidjson::Document::AllocatorType& allocator)
{
    Json json;
    
    rapidjson::Value value(rapidjson::kObjectType);
    MapContainer& current = getMap();
    
    // add all walls and positions to the json object
    // from the current Map
    for(Building& building : current._buildings)
    {
        rapidjson::Value bValue(rapidjson::kObjectType);
        rapidjson::Value arrWalls(rapidjson::kArrayType);
        
        for(ColRectangle& wall : building.Wall)
        {
            const sf::Color& wall_color = wall.getFillColor();
            
            rapidjson::Value bWall(rapidjson::kObjectType);
            
            bWall.AddMember(
                "wall_size",
                json.getVector(wall.getSize(), allocator), 
                allocator);
            bWall.AddMember(
                "wall_position", 
                json.getVector(wall.getPosition(), allocator), 
                allocator);
            bWall.AddMember(
                "wall_color",
                json.getColor(wall_color, allocator),
                allocator);
            
            // valueBuilding.AddMember("wall", bWall, allocator);
            arrWalls.PushBack(bWall, allocator);
        }
        
        ColRectangle& door                = building.Door;
        const sf::Color& door_color       = door.getFillColor();
        
        bValue.AddMember("walls", arrWalls, allocator);
        
        bValue.AddMember(
            "door_color", 
            json.getColor(door_color, allocator), 
            allocator);
        
        bValue.AddMember(
            "door_size", 
            json.getVector(door.getSize(), allocator), 
            allocator);
        
        bValue.AddMember(
            "door_position",
            json.getVector(door.getPosition(), allocator),
            allocator);
        
        value.AddMember("building", bValue, allocator);
    }
    
    return value;
}

void MapContainer::update(Player& player)
{
    MapContainer& current = getMap();
    
    // check for collision for each wall inside the Building class
    for(Building& build : current._buildings)
    {
        // Check each wall for collisions and handle them. 
        for(ColRectangle& wall : build.Wall)
            World::colHandler.handleObstacle(player, wall);
        
        if(World::colHandler.collision(player, build.Door))
        {
            build.Door.changeColor(sf::Color::Green);
            
            // change map when player is moving to the door.
            if(_currentLevel == LEVEL::House)
            {
                _currentLevel = LEVEL::World;
            }
            else if(_currentLevel == LEVEL::World)
            {
                _currentLevel = LEVEL::House;
            }
        }
        else
        {
            build.Door.changeColor(sf::Color::Yellow);
        }
    }
    
}

void MapContainer::insertMap(const unsigned int& level)
{
    insertMap(static_cast<MapContainer::LEVEL>(level));
}


void MapContainer::insertMap(MapContainer::LEVEL level)
{
    Ptr pointer(new MapContainer());
    auto inserted = _mapHolder.insert(std::make_pair(level, std::move(pointer)));
    assert(inserted.second);
}

void MapContainer::loadMap(MapContainer::LEVEL level)
{
    std::cout << "Loading level " << level << '\n';
    _currentLevel = level;
}

void MapContainer::loadMap(const unsigned int& level)
{
    loadMap(static_cast<MapContainer::LEVEL>(level));
}

void MapContainer::setSpawnPosition(const sf::Vector2f& newPosition)
{
    getMap()._spawnPosition = newPosition;
}

void MapContainer::setSpawnPosition(const rapidjson::Value& obj)
{
    Json json;
    setSpawnPosition(json.getVector(obj["spawn_position"]));
}

const sf::Vector2f& MapContainer::getSpawnPosition()
{
    return getMap()._spawnPosition;
}

