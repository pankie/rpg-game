/* 
 * File:   File.cpp
 * Author: Fredrik
 * 
 */

#include <sstream>

#include "File.hpp"

std::string File::readFile(const std::string& filePath)
{
    // The file
    std::ifstream     file;
    
    // where we store the string
    std::string       fileContent;
    
    // the buffer
    std::stringstream buffer;
    
    // check if file exists
    file.open(filePath);
    if(file.is_open())
    {
        buffer << file.rdbuf();
        fileContent = buffer.str();
        
        file.close();
        
    }
    else
    {
        std::cerr << "Couldn't read file " << filePath << '\n';
    }
    
    return fileContent;
}

void File::writeFile(const std::string& filePath, const std::string& text)
{
    std::ofstream file;
    file.open(filePath);
    if(file.is_open())
    {
        file << text;
        file.close();
    }
    else
    {
        std::cerr << "Couldn't open file " << filePath << '\n';
    }

    
}