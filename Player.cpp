/* 
 * File:   Player.cpp
 * Author: Fredrik
 * 
 * Created on den 27 oktober 2014, 05:34
 */

#include "Player.hpp"
#include "Application.hpp"

Player::Player()
: _character("./character.png")
, Collisionable({0, 24, 28, 24})
{
    // sets origin of the player
    setOrigin(32 / 2.f, 48 / 2.f);
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(_character, states);
}

void Player::handleMovement(const sf::Event::KeyEvent& key, bool isPressed)
{
    
    switch(key.code)
    {
    case sf::Keyboard::W:
        Up = isPressed;
        break;
    case sf::Keyboard::S:
        Down = isPressed;
        break;
    case sf::Keyboard::A:
        Left = isPressed;
        break;
    case sf::Keyboard::D:
        Right = isPressed;
        break;
    default:
        break;
    }
}

void Player::handleMovement()
{
    if(Up)
    {
        // sets the new velocity for the object.
        setVel({0, -100});
    }
    else if(Down)
    {
        setVel({0, 100});
    } 
    else if(Right)
    {
        setVel({100, 0});
    }  
    else if(Left)
    {
        setVel({-100, 0});
    }
    else
    {
        setVel({0, 0});
    }
}

void Player::handleEvents(const sf::Event& event)
{
    switch(event.type)
    {
    case sf::Event::KeyPressed:
        handleMovement(event.key, true);
        break;
    case sf::Event::KeyReleased:
        handleMovement(event.key, false);
        break;
    default:
        break;
    }
}

void Player::update(const sf::Time& dt)
{
    // handle the player's movement, if the menu is activated we disable his movement.
    if(!Menu::MenuIsOpen)
        handleMovement();

    
    // The animation cycle
    _character.updateAnimationCycle(dt, getVel());
    move(dt.asSeconds() * getVel());
}
