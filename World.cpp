#include "World.hpp"
#include "Application.hpp"

// static
CollisionHandler World::colHandler;

World::World(sf::RenderWindow& window)
: _window(window)
, _player() 

// View for the player
, _playersView()

// Handles collisions and draws the map
, _map(_player)
{
    _player.setPosition(0, 0);
    
    // sets the view to player's current position
    _playersView.setCenter(_player.getPosition());
    _playersView.setSize(480, 320);
    _window.setView(_playersView);
    
}

void World::updateViewPosition()
{
    _playersView.setCenter(_player.getPosition());
    _window.setView(_playersView);
}

void World::handleEvents(const sf::Event& event)
{
    _player.handleEvents(event);
    
    // handling events for debugging, for now.
    _map.handleEvents(event);
}

void World::render()
{
    // draw the map
    _window.draw(_map);
    
    // draw the player
    _window.draw(_player);
}

void World::update(const sf::Time& deltaTime)
{
    _player.update(deltaTime);
    _map.update(deltaTime);
    

    updateViewPosition();
}