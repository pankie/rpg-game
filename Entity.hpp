/* 
 * File:   Entity.hpp
 * Author: Fredrik
 *
 * Created on den 29 oktober 2014, 15:34
 */

#ifndef ENTITY_HPP
#define	ENTITY_HPP

#include <SFML/Graphics.hpp>

class Entity : public sf::Transformable
{
public:
    virtual void update(const sf::Time& dt);
    
    // set the velocity of the object.
    void setVel(const sf::Vector2f& v);
    const sf::Vector2f getVel();
private:
    sf::Vector2f velocity;
};

#endif	/* ENTITY_HPP */

