/* 
 * File:   Character.hpp
 * Author: Fredrik
 *
 */

#ifndef CHARACTER_HPP
#define	CHARACTER_HPP

#include "Animation.hpp"
#include <SFML/Graphics.hpp>

class Character : public Animation, public sf::Drawable
{
public:
    Character(const std::string& filePath);

public:
    // If player from network walks, he needs to cycle too.
    //void         updateAnimationCycle(const Player::Information& info);   
    void         updateAnimationCycle(const sf::Vector2i& source);
    void         updateAnimationCycle(const sf::Time& dt, const sf::Vector2f& velocity);
    virtual void nextFrame();
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
 
private:
    sf::Texture _texture;
    
    // For the character's movement.
    enum Direction { Down, Left, Right, Up };
};

#endif	/* CHARACTER_HPP */

