/* 
 * File:   Character.cpp
 * Author: Fredrik
 * 
 */

#include "Character.hpp"
#include "Application.hpp"

Character::Character(const std::string& filePath)
: Animation()
{
    _texture.loadFromFile(filePath);
    Animation::sprite.setTexture(_texture);
    Animation::sprite.setTextureRect(
        sf::IntRect(source.x * 32, source.y * 48, 32, 48));
}

void Character::updateAnimationCycle(const sf::Time& dt, const sf::Vector2f& velocity)
{
    // Keeps the animation cycle from not going too fast.
    timeFrame += dt;
    
        // If the character is moving upwards
        if(velocity.y < 0)
        {
            // Change the vector y to 4
            source.y = Direction::Up;
            // If enough time has elapsed, go for the next frame
            if(timeFrame.asSeconds() > 0.25)
            {
                nextFrame();         
            }
        }
        else if(velocity.y > 0)
        {
            source.y = Direction::Down;
            if(timeFrame.asSeconds() > 0.15)
            {
                nextFrame();
            }
        }
        else if(velocity.x > 0)
        {
            source.y = Direction::Right;
            if(timeFrame.asSeconds() > 0.15)
            {
                nextFrame();
            }
        }  
        else if(velocity.x < 0)
        {
            source.y = Direction::Left;
            if(timeFrame.asSeconds() > 0.15)
            {
                nextFrame();
            }
        }
        else
        {
            source.x = 0;
        }

        // Update sprite
        sprite.setTextureRect(sf::IntRect(source.x * 32, source.y * 48, 32, 48));
}

void Character::nextFrame()
{
    // Make sure the current frame doesn't get out of bounds on the texture.
    if(source.x >= 3)
        source.x = 0;
    else
        source.x++;

    // reset the timer.
    timeFrame = sf::Time::Zero;
}

void Character::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(Animation::sprite, states);
}
