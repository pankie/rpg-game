/* 
 * File:   MapContainer.hpp
 * Author: Fredrik
 *
 */

#ifndef MAPCONTAINER_HPP
#define	MAPCONTAINER_HPP

#include <SFML/Graphics.hpp>
#include "ColRectangle.hpp"
#include <memory>
#include <vector>
#include <cassert>
#include "rapidjson/document.h"
#include "Json.hpp"

class MapContainer : public sf::Drawable, public sf::Transformable
{
public:
    typedef std::unique_ptr<MapContainer> Ptr;
    
    // Building object
    struct Building
    {
        // adds a wall to the Building object.
        void addWall(const ColRectangle& wall);
        void addWall(ColRectangle& wall);
        
        // adds a door to the Building object.
        void addDoor(const ColRectangle& door);
        
        std::vector<ColRectangle>   Wall;
        ColRectangle                Door;
    };
    
    
    enum LEVEL
    {
        World,
        House
    };
    
public:
    
    MapContainer();
    
    // loads the maps from json document
    void reloadMaps(const rapidjson::Document& document);
    
    // loads all maps from json document
    void addMaps(const rapidjson::Document& document);
    
    // returns all buildings from current selected object
    // as json object.
    rapidjson::Value getJsonBuilding(rapidjson::Document::AllocatorType& allocator);
    
    // returns the floor from the current building
    rapidjson::Value getJsonFloor(rapidjson::Document::AllocatorType& allocator);
    
    // adds building to a current level.
    void addBuilding(const Building& building);
    
    // adds the floor to the current level.
    void addFloor(const sf::RectangleShape& rectangle);
    
    // adds the floor to the current level.
    void addFloor(const rapidjson::Value& val);
    
    // adds a building to the current level.
    void addBuilding(const rapidjson::Value& val);
    
    // adds a new spawn position for the current level
    void setSpawnPosition(const sf::Vector2f& newPosition);
    
    // adds a new spawn position for the current level
    void setSpawnPosition(const rapidjson::Value& obj);
    
    // returns the spawn position for the current level
    const sf::Vector2f& getSpawnPosition();
    
    // changes the map
    void loadMap(MapContainer::LEVEL level);
    
    // changes the map
    void loadMap(const unsigned int& level);
    
    // deletes the map
    void deleteMap(const unsigned int& level);
    
    // deletes the map
    void deleteMap(MapContainer::LEVEL level);
    
    // adds a new MapContainer to the class
    void insertMap(MapContainer::LEVEL level);
    
    // adds a new MapContainer to the class
    void insertMap(const unsigned int& level);
    
    // returns the current map/level
    MapContainer& getMap() const;
    
    // prevents player from going through buildings
    void update(Player& player);
    
    // Switch to a another map.
    // for debugging only.
    void debug_toggleMap();
    
private:
    // draws the current map that has been loaded by MapContainer::loadMap
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    
private:
    
    // contains every map in a pointer.
    std::map<MapContainer::LEVEL, Ptr>          _mapHolder;
    
    // stores all the buildings
    std::vector<Building>                       _buildings;
     
    // The floor of the level
    sf::RectangleShape                          _floor;
    
    // Spawn position for the player
    sf::Vector2f                                _spawnPosition;
    
    // Used to draw the current level or map the player is on.
    LEVEL                                       _currentLevel;
    
    
};

#endif	/* MAPCONTAINER_HPP */

