/* 
 * File:   Button.cpp
 * Author: Fredrik
 * 
 */

#include "Button.hpp"
#include "Menu.hpp"
#include <iostream>

Button::Button(sf::RenderWindow& window)
: _window(window)
, _selected(0)
{
}

void Button::addButton(const sf::Vector2f& position, const sf::Vector2f& size)
{
    sf::RectangleShape rectangle(size);
    rectangle.setOrigin(size.x / 2.f, size.y / 2.f);
    rectangle.setFillColor(sf::Color(125, 180, 255));
    rectangle.setPosition(position);
    _buttons.push_back(std::move(rectangle));
    std::cout << "sizeOf Buttons: " << _buttons.size() << std::endl;
}

void Button::update(const sf::Time& dt)
{
    if(Menu::MenuIsOpen)
    {
        for(unsigned int i = 0; i < _buttons.size(); i++)
        {
            if(i == _selected)
            {
                if(_buttons[_selected].getFillColor() != sf::Color::Blue)
                {
                    _buttons[_selected].setFillColor(sf::Color::Blue);
                }
            }
            else
            {
                if(_buttons[i].getFillColor() == sf::Color::Blue)
                {
                    _buttons[i].setFillColor(sf::Color(125, 180, 255));
                }
            }
        }
    }
}

void Button::selectButton()
{
    std::cout << "Pressed on " << _selected << '\n';
}

void Button::selectNextButton()
{
    if(_selected < _buttons.size() - 1)
        _selected += 1;
}

void Button::selectPreviousButton()
{
    if(_selected > 0)
        _selected -= 1;
}

void Button::toggleColor(sf::RectangleShape& rectangle)
{
    if(rectangle.getFillColor() == sf::Color::Blue)
        rectangle.setFillColor(sf::Color(125, 180, 255));
    else
        rectangle.setFillColor(sf::Color::Blue);
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    for(const sf::RectangleShape& button : _buttons)
    {

        target.draw(button, states);
    }
}