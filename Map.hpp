/* 
 * File:   Map.hpp
 * Author: Fredrik
 *
 */

#ifndef MAP_HPP
#define	MAP_HPP

#include <SFML/Graphics.hpp>
#include "ColRectangle.hpp"
#include "Player.hpp"
#include "MapContainer.hpp"
#include "Json.hpp"

// Drawable maps
class Map : public sf::Drawable, public sf::Transformable
{
public:
    
    Map(Player& player);
    
    // set spawn position
    void setSpawnPosition(const sf::Vector2f& pos);
    
    void update(const sf::Time& dt);
    void handleEvents(const sf::Event& event);
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    
    // initialize all maps
    void         init();
    
    // saves all maps
    // contains ugly code.
    void         saveWorlds();
    
    // loads all maps
    void         loadWorlds();
    
    
public:
    
    // spawn position for player.
    sf::Vector2f            _spawnPosition;
    
    // Used to move player to a new position
    Player&                 _player;
    
    MapContainer            _mapcontainer;
    
    Json                    _json;
    
};

#endif	/* MAP_HPP */

