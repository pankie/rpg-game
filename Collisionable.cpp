
#include "Collisionable.hpp"

Collisionable::Collisionable()
{
}

Collisionable::Collisionable(const sf::Vector2f& size)
: bounds(0, 0, size.x, size.y)
{
}

Collisionable::Collisionable(const sf::FloatRect& rect)
: bounds(rect)
{
    
}

void Collisionable::setBounds(const sf::FloatRect& rect)
{
    bounds = rect;
}

sf::FloatRect Collisionable::getGlobalBounds() const
{
    return getTransform().transformRect(bounds);
}

bool Collisionable::collision(Collisionable& o)
{
    if(getGlobalBounds().intersects(o.getGlobalBounds()))
        return true;
    
    return false;
}
